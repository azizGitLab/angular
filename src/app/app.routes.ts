/**
 * Created by idrissi on 22.05.2018.
 */
import {Routes} from '@angular/router';
import { ProductComponent } from './product/product.component';
import { EventComponent } from './event/event.component';
import { AppComponent } from './app.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import { HttpClientComponent } from './http-client/http-client.component';
import { PageNotFundomponentComponent } from './page-not-fundomponent/page-not-fundomponent.component';

import {RouterComponent} from "./router/router.component";
import { DetailsComponent } from './details/details.component';
import { StepperComponent } from './stepper/stepper.component';
import { ArchitectureComponent } from './architecture/architecture.component';
import { PixabayComponent } from './pixabay/pixabay.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { RegistrierungComponent } from './registrierung/registrierung.component';
import { AuthGuardService } from './service/auth-guard.service';
import { ListUserComponent } from './list-user/list-user.component';
import { UserFormComponent } from './user-form/user-form.component';

export const approutes: Routes = [

    // Hier sind die Route erstellt, aber noch nicht verwendet in der Applikation
    {path: 'approot', component: AppComponent},
    {path: 'event', component: EventComponent},
    {path: 'service', component: ProductComponent},
    {path: '', redirectTo: '/home', pathMatch:'full'},
   //  {path: '', redirectTo: '/login', pathMatch:'full'},
    {path: 'home', component: HomeComponentComponent},
    {path: 'arch', component: ArchitectureComponent},
    {path: 'httpclient', component: HttpClientComponent},
    {path: 'router', component: RouterComponent},
    {path: 'stepper', component: StepperComponent},
    {path: 'details/:id',component: DetailsComponent},  
    {path: 'pixabay',component: PixabayComponent},
    {path: 'login',component: ReactiveFormComponent},
    {path: 'register',component: RegistrierungComponent},
    {path: 'listuser',component: ListUserComponent},
    {path: 'formuser',component: UserFormComponent},
    
   
    
    {path: '**', component: PageNotFundomponentComponent}
   
    ];
