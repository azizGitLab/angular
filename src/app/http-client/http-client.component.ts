import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Post} from './post';
import {Flug} from './flug';
import  {Observable}  from 'rxjs';
import { FormsModule } from '@angular/forms';



@Component({
  selector: 'app-http-client',
  templateUrl: './http-client.component.html',
  styleUrls: ['./http-client.component.css']
})
export class HttpClientComponent implements OnInit {

  public componentHttpClient = 'http://www.angular.at/api/flug';


  ngOnInit() {
  }


  //readonly JSON_URL = 'https://jsonplaceholder.typicode.com/posts';
  //readonly JSON_URL = 'https://jsonplaceholder.typicode.com/users';
  readonly FLUG_URL ='http://www.angular.at/api/flug';

  //posts: any;

  isAdressChecked =false;

  isIdChecked = false;

  postsAny: Observable<any>;

  users: any[];

  newPost:  Observable<any>;
  constructor(private http: HttpClient) {}

  //getPosts(){
   // this.posts = this.http.get(this.JSON_URL);
 // }




/*
  createPost(){
    const  data: Post = {
      id: null,
      userId:23,
      title: "My new Post",
      body: "Hello Post!"
    }
    this.newPost = this.http.post(this.JSON_URL, data);
     //   .map(post => post.title)  ;
  }
  */

// FlugSuchen

  fluege: Observable<Flug[]>;


//new


  von: string = "Graz";
  nach: string = "Hamburg";
  flights: Array<Flug> = [];
  selectedFlight: Flug;


  select(flight: Flug): void {
    this.selectedFlight = flight;
  }




  search(): void {

    let url = 'http://www.angular.at/api/flug';

      let params = new HttpParams()
        .set('abflugOrt', this.von)
        .set('zielOrt', this.nach);


    this.fluege = this.http.get<Flug[]>(url, {params});

  }

}
