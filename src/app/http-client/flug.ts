/**
 * Created by idrissi on 25.05.2018.
 */

import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface Flug {
    id: number;
    abflugort: string;
    zielort: string;
    datum: string;
    // Datum im ISO-Format
}