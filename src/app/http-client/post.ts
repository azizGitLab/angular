/**
 * Created by idrissi on 24.05.2018.
 */
export interface Post{
    id: number;
    userId: number;
    title: string;
    body: string;
}