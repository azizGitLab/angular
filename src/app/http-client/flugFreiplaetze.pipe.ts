/**
 * Created by idrissi on 18.06.2018.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'freiePlaetzePipe'})
export class FreiePlaetzePipe implements PipeTransform {

    transform(freiePlaetze: number): String {

        if (freiePlaetze > 0) {
            return freiePlaetze+"";
        }
        else {
            return " kein Platz frei";
        }

    }
}