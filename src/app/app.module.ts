import { BrowserModule } from '@angular/platform-browser';
//import {NoopAnimationsModule} from '@angular/platform-browser/animations'
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatStepperModule} from '@angular/material/stepper';
import { MatAutocompleteModule } from '@angular/material';

import {MatExpansionModule} from '@angular/material/expansion';

import {MatCardModule} from '@angular/material/card';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { FreiePlaetzePipe } from './http-client/flugFreiplaetze.pipe';


import {RouterModule} from '@angular/router';

import {approutes} from './app.routes';



import {MyserviceService} from './service/myservice.service';
import { EventComponent } from './event/event.component';

import {HttpClientModule} from '@angular/common/http';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatBadgeModule} from '@angular/material/badge';
import {MatTabsModule} from '@angular/material/tabs';
import { PageNotFundomponentComponent } from './page-not-fundomponent/page-not-fundomponent.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import { HttpClientComponent } from './http-client/http-client.component';
import { MaterialComponent } from './material/material.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterComponent } from './router/router.component';
import { DetailsComponent } from './details/details.component';
import { FilterPipe } from './filter.pipe';
import { StepperComponent } from './stepper/stepper.component';
import { ArchitectureComponent } from './architecture/architecture.component';
import { PixabayComponent } from './pixabay/pixabay.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { RegistrierungComponent } from './registrierung/registrierung.component';
import { ListUserComponent } from './list-user/list-user.component';


import { MatPaginatorModule } from '@angular/material/paginator';
import { UserFormComponent } from './user-form/user-form.component'; 




//import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
//import {MenuModule} from 'primeng/menu';




@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    EventComponent,
    PageNotFundomponentComponent,
    HomeComponentComponent,
    HttpClientComponent,
    MaterialComponent,
    FreiePlaetzePipe,
    RouterComponent,
    DetailsComponent,
    FilterPipe,
    StepperComponent,
    ArchitectureComponent,
    PixabayComponent,
    ReactiveFormComponent,
    RegistrierungComponent,
    ListUserComponent,
    UserFormComponent,
    
  ],
  
  imports: [
    RouterModule.forRoot(approutes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    //NoopAnimationsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatInputModule,
    MatMenuModule,
    MatDatepickerModule,
    MatDividerModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatBadgeModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatExpansionModule,
    MatTabsModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatPaginatorModule,
  

  ],
  
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],

  //providers: [MyserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
