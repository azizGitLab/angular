import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PixabayService {

  constructor(private http: HttpClient) { }


  getImages(imagekey) {
    
    return this.
    http.get('https://pixabay.com/api/?key=10572531-858e6b0bda4d79ad27d11aa10&q='+imagekey+"&per_page=12");
    
  }


}
