import { Component, OnInit,ViewChild } from '@angular/core';
import { UserService } from '../service/user.service';
import { User } from '../User';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { DataSource } from '../../../node_modules/@angular/cdk/collections';
import { Observable } from '../../../node_modules/rxjs';
import {CdkTableModule} from '@angular/cdk/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  //private users:any;

  
  private users: User[];


  
  constructor(private userService:UserService, private router: Router) {


   }

 
  
  ngOnInit() {
    
   this.userService.getUsers().subscribe((data)=>{
      console.log(data);
      this.users=data;
   
    }, (error)=>{
      console.log(error)
    })
 
  }



  deleteUser(user:User){

    this.userService.deleteUser(user.id)
    .subscribe( data => {
      this.users.splice(this.users.indexOf(user),1);
      console.log('deleted User Id: '+user.id + ' Name: '+user.lname + ' Vorname: '+user.fname);
  
    }, (error)=>{
      console.log(error);
    })
 
  }


  updateUser(user){

    this.userService.setter(user);
    this.router.navigate(['/formuser']);

  }

  createUser(){
    let user = new User();
    this.userService.setter(user);
    this.router.navigate(['/formuser'])
  }

}

