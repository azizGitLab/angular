import { Component } from '@angular/core';
import { AuthServiceService } from './service/auth-service.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ILogin } from './reactive-form/ILogin';






@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'Angular 7';


  public currentDate = new Date();

 
  public loginSuccess = "false";

  public isCollapsed: boolean = false;


  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }


}
