import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from "@angular/router";

import { trigger,style,transition,animate, state,keyframes,query,stagger } from '@angular/animations';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  animations: [
    trigger('exampleAnimation', [

      state('small', style({

        transform: 'scale(1)',

      })),

      state('large', style({

        transform: 'scale(1.6)',
      })),
      transition('small  => large', animate('400ms ease-in')),
    ]),
  ]

})
export class DetailsComponent implements OnInit {

   users: any;
   

  constructor(private route: ActivatedRoute, private data: DataService) {
    this.route.params.subscribe(params => this.users = params.id);
  }

  ngOnInit() {
    this.data.getUser(this.users).subscribe(data => this.users = data);
  }


  state: string = 'small'
  animateMe(){
    this.state = (this.state === 'small' ? 'large' : 'small');
  }

}
