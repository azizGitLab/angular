import { Component, OnInit, EventEmitter, Output} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ILogin } from './ILogin';
import { Router } from '@angular/router';
import { AuthServiceService } from '../service/auth-service.service';




@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  model: ILogin = { benutzerName: "admin", password: "admin" };


  loginForm: FormGroup;

  loginPost: any;


  public loginSuccess : boolean = false

  @Output() public childEvent = new EventEmitter();

  errorMessage: string;
  
  returnUrl: string;

  constructor(private formBuilder: FormBuilder,private router: Router, public authService: AuthServiceService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      benutzerName: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl = '/home';
    this.authService.logout();
  }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    login() {

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      else{
        if(this.f.benutzerName.value == this.model.benutzerName && this.f.password.value == this.model.password){
          console.log("Login successful");
          //this.authService.authLogin(this.model);
          localStorage.setItem('isLoggedIn', "true");
          localStorage.setItem('token', this.f.benutzerName.value);
          this.router.navigate([this.returnUrl]);
          
          this.childEvent.emit(localStorage.getItem ('isLoggedIn'));
        }
        else{
          this.errorMessage = "please check your username and password";
        }
      }    
     
  }


}
