import { FormControl, AbstractControl } from '@angular/forms';


export function confirmPassValidator(controlpass: AbstractControl){
    if(controlpass && (controlpass.value !== null || controlpass.value !== undefined)){
        const confirmpassValue = controlpass.value;

        const passControl = controlpass.root.get('password');
        if(passControl){
            const passValue = passControl.value;
            if(passValue !== confirmpassValue){
                return{
                    isError: true
                };
            }
        }
    }
    return null;
}