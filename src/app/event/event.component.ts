import { Component } from '@angular/core';
import {MyserviceService} from '../service/myservice.service';
import {Subscription, Observable} from 'rxjs';


@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent {

   name ="initial";

  public isCollapsed : boolean = true;
  toggleCollapse(){
    this.isCollapsed = !this.isCollapsed;
  }
  

  public dateMessage: string;
   constructor() {
     
     setInterval( ()=>{
      let currentDate = new Date();
      this.dateMessage = currentDate.toDateString() + ' ' + currentDate.toLocaleTimeString();}  ,2000)
   }


 
  }
