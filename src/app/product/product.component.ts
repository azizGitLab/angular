import { Component, OnInit } from '@angular/core';
import {MyserviceService} from '../service/myservice.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  })
  
  //ab version 6 ist nicht mehr nötig siehe MyserviceService ( providedIn: 'root',)
  // providers: [MyserviceService]

export class ProductComponent implements OnInit {

  public serviceValue = 'Service';

    productService: MyserviceService;

    recentlyProducts: string[];
   
    public nbOfProducts: Number;                   

  constructor(myproductService: MyserviceService) {
      this.productService = myproductService;
  }
  ngOnInit() {
    this.recentlyProducts = this.productService.getAllMovies();
    this.nbOfProducts = this.recentlyProducts.length;
  }
  addOneItem(productTitle){
      this.productService.addItem(productTitle);
      this.nbOfProducts = this.recentlyProducts.length;
  }
  removeOneItem(){
    this.productService.removeItem();
    this.nbOfProducts = this.recentlyProducts.length;
}

}
