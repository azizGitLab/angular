import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PixabayService } from '../pixabay.service';
import { Observable } from '../../../node_modules/rxjs/internal/Observable';

@Component({
  selector: 'app-pixabay',
  templateUrl: './pixabay.component.html',
  styleUrls: ['./pixabay.component.css']
})
export class PixabayComponent implements OnInit {

  images: any;

  imagekey = "marrakesch";
  
  constructor(private pixaSevice: PixabayService) { }

  searchImages(imagekey) {
    
    return this.pixaSevice.getImages(imagekey).subscribe(result => this.images = result);
  }
 
  ngOnInit() {
  }

}
