import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(users: any[], searchText: string): any[] {

    console.log("users "+users);
   

    if(!users || !searchText ){

      return users;
    }
    
   // console.log("Bauch".toLowerCase().indexOf(searchText.toLocaleLowerCase());
   // Gibt die Elemente eines Arrays zurück, die die in einer Callback-Funktion angegebene Bedingung erfüllen.

    /**
      * Returns the elements of an array that meet the condition specified in a callback function.
      * @param callbackfn A function that accepts up to three arguments. The filter method calls the callbackfn function one time for each element in the array.
      * @param thisArg An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
      */

return users.filter( us => 
       us.name.toLowerCase().indexOf(searchText.toLocaleLowerCase()) !== -1);

   }
}