import { Component, OnInit } from '@angular/core';

import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';


@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.css'],
  animations: [
      
    trigger('explanerAnimation', [
      transition('* => *', [
        query('.comp', style({ opacity: 0,  transform: 'translateX(-40px)' })),

        query('.comp', stagger('1000ms', [
          animate('300ms 1.2s ease-out', style({opacity: 1, transform: 'translateX(0)'}))

        ])) 
      ])
    ])

  ]
 
})
export class HomeComponentComponent  implements OnInit  {


  constructor() {}

  ngOnInit() {
  

    
  }

  



}

