import { Component, OnInit } from '@angular/core';
import { User } from '../User';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  private user: User;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.user = this.userService.getter();
  }

  handelForm() {
    if (this.user.id==undefined) {  
      this.userService.createUser(this.user).subscribe((user) => {
        console.log(user);
        this.router.navigate(['/listuser']);
      }, (error) => {
        console.error();
      });
    } else {
      this.userService.updateUser(this.user).subscribe((user) => {
        console.log(user);
        this.router.navigate(['/listuser']);
      }, (error) => {
        console.error();
      });
    }
  }

}
