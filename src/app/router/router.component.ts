import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';


@Component({
  selector: 'app-router',
  templateUrl: './router.component.html',
  styleUrls: ['./router.component.css'],
  animations: [
      
    trigger('explanerAnimation', [
      transition('* => *', [
        query('.col', style({ opacity: 0,  transform: 'translateX(-40px)' })),

        query('.col', stagger('5500ms', [
          animate('800ms 1.2s ease-out', style({opacity: 1, transform: 'translateX(0)'}))

        ])) 
      ])
    ])

  ]
})
export class RouterComponent implements OnInit {

  routerTitel = 'Routing & Navigation';

 

   public isrouterValue : boolean = true;
   toggleRouterValue(){
     this.isrouterValue = !this.isrouterValue;
   }

  

   constructor( ) { }

  ngOnInit() {
  }

}
