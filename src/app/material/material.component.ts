import { Component, OnInit } from '@angular/core';
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';
import { Observable } from 'rxjs';
import { DataService } from '../data.service';



@Component({
    selector: 'app-material',
    templateUrl: './material.component.html',
    styleUrls: ['./material.component.css'],

    animations: [
        trigger('listStagger', [
            transition('* <=> *', [
                query(
                    ':enter',
                    [
                        style({ opacity: 0, transform: 'translateY(-15px)' }),
                        stagger('150ms',
                            animate('550ms ease-out',style({ opacity: 1, transform: 'translateY(0px)' })
                            )
                        )
                    ],
                    { optional: true }
                ),
                query(':leave', animate('50ms', style({ opacity: 0 })), {
                    optional: true
                })
            ])
        ])
    ]
})

export class MaterialComponent implements OnInit {

    isIdChecked = false;

    users: any;


    constructor(private data: DataService) { }

    ngOnInit() {
      //  this.data.getUsers().subscribe(data => this.users = data);
    }

    getUsersData(){
        this.data.getUsers().subscribe(data => this.users = data);
    }

}



