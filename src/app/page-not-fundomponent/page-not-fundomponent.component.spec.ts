import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotFundomponentComponent } from './page-not-fundomponent.component';

describe('PageNotFundomponentComponent', () => {
  let component: PageNotFundomponentComponent;
  let fixture: ComponentFixture<PageNotFundomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNotFundomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFundomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
