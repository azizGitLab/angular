import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-fundomponent',
  templateUrl: './page-not-fundomponent.component.html',
  styleUrls: ['./page-not-fundomponent.component.css']
})
export class PageNotFundomponentComponent implements OnInit {

  public componenterror = 'The page does not exist';

  constructor() { }

  ngOnInit() {
  }

}
