import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { User } from '../User';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = 'http://localhost:3000/userApi'

  user: User;


  constructor(private http: HttpClient) { }


  getUsers() {

    return this.http.get<User[]>(this.baseUrl + '/users');
  }


  deleteUser(id: Number) {

    return this.http.delete(this.baseUrl + '/user/' + id)

  }


  updateUser(user) {

    return this.http.put(this.baseUrl + '/user', user);

  }

  createUser(user) {

    return this.http.post(this.baseUrl + '/user', user);

  }


  setter(user: User) {
    this.user = user;
  }

  getter() {
    return this.user;
  }


}
