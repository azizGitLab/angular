import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class MyserviceService {

    items: string[];

    getAllMovies() {
        this.items = ['product 1','product 2','product 3'];
        return this.items;
    }

    addItem(movieItemTitel) {
        if (movieItemTitel) {
            this.items.push(movieItemTitel);
        }
    }

    removeItem(){
        this.items.pop();
    }

}
