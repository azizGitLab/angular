import { Component, OnInit } from '@angular/core';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {

  public titleMaterial = "https://material.angular.io/";

  isLinear = false;

  public isFormfilled : boolean = false;
  checkFormfilled(){
    this.isFormfilled = !this.isFormfilled;
  }




  formGroup1: FormGroup;
  formGroup2: FormGroup;
  formGroup3: FormGroup;

  formGr: FormGroup;

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.formGroup1 = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.formGroup2 = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.formGroup3 = this._formBuilder.group({
      thirtCtrl: ['', Validators.required]
    });

  
    
  }

  



}

